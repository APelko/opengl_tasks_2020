#ifndef OPENGL_TASKS_2020_PERLINNOISE_H
#define OPENGL_TASKS_2020_PERLINNOISE_H

#include <vector>

class PerlinNoise {
    std::vector<int> p;
public:
    // Initialize with the reference values for the permutation vector
    PerlinNoise();

    // Get a noise value, for 2D images z can have any value
    double noise(double x, double y, double z);

private:
    double fade(double t);

    double lerp(double t, double a, double b);

    double grad(int hash, double x, double y, double z);
};


#endif //OPENGL_TASKS_2020_PERLINNOISE_H
