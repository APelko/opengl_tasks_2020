#include "common/Application.hpp"
#include "common/Mesh.hpp"
#include "common/ShaderProgram.hpp"

#include <vector>



class SampleApplication : public Application {
public:
    MeshPtr meshPtr;

    ShaderProgramPtr shaderProgramPtr;

    void makeScene() override {
        Application::makeScene();

        _cameraMover = std::make_shared<FreeCameraMover>();
        shaderProgramPtr = std::make_shared<ShaderProgram>("695PelkoData1/shader.vert", "695PelkoData1/shader.frag");
        meshPtr = makeRelief(200,5,10);
        glm::mat4 m =  glm::translate(glm::mat4(1.0f), glm::vec3(-10.0, -10.0, -10.0));
        meshPtr->setModelMatrix(m);
    }

    void draw() override {
        Application::draw();
        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);
        glViewport(0, 0, width, height);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        shaderProgramPtr->use();

        shaderProgramPtr->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        shaderProgramPtr->setMat4Uniform("projectionMatrix", _camera.projMatrix);
        shaderProgramPtr->setMat4Uniform("modelMatrix", meshPtr->modelMatrix());
        meshPtr->draw();
    }
};

int main() {
    SampleApplication app;
    app.start();

    return 0;
}