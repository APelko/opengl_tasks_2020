#version 330

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;

layout(location = 0) in vec3 vertexPosition;
layout(location = 1) in vec3 vertexNormal;

out vec4 color;

void main()
{
    gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4(vertexPosition, 1.0);
    color = vec4(((2 * vertexPosition[2] - 1) * 50) / 255, ((vertexPosition[2] - 1)* 50) / 255, ((vertexPosition[2] - 2) * 50) / 255, 1.0f);
}
